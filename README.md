The 2D FEM simulator in Python is provided in folder 'code'. 
This is based on the article: Nonlinear 2D Finite Element Modeling: Cyclic Energy Storage in Salt Caverns with Creep Deformation Physics 
Authors: Artur Makhmutov, Kishan Ramesh Kumar, Chris Spiers, Hadi Hajibeygi
Principle Investigator: Hadi Hajibeygi, H.Hajibeygi@TUDelft.nl
Code developer: Artur Makhmutov
Artur's MSc thesis was also done on this subject. 
This code considers elastic and creep deformation of 2D salt caverns. 
This code is our lab release 0.0
We are currently adding viscoplastic deformation into it and fine tune all.

Stay connected; or contact Hadi Hajibeygi, to get notifications of the updates.

Professor Hadi Hajibeygi, 10 October 2020, Delft.
