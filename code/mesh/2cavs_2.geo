// Gmsh project created on Wed Apr 15 14:08:51 2020
//+
Point(1) = {-0.5, -1, 0, 1.0};
//+
Point(2) = {-0.5, 1, 0, 1.0};
//+
Point(3) = {0.5, 1, 0, 1.0};
//+
Point(4) = {0.5, -1, 0, 1.0};
//+
Point(5) = {-0.5, 0.3, 0, 0.1};
//+
Point(6) = {-0.4, 0.2, 0, 0.1};
//+
Point(7) = {-0.5, 0.2, 0, 0.1};
//+
Point(8) = {-0.5, -0.2, 0, 0.1};
//+
Point(9) = {-0.4, -0.2, 0, 0.1};
//+
Point(10) = {-0.5, -0.3, 0, 0.1};
//+
Point(11) = {0.5, 0.3, 0, 0.1};
//+
Point(12) = {0.5, 0.2, 0, 0.1};
//+
Point(13) = {0.4, 0.2, 0, 0.1};
//+
Point(14) = {0.5, -0.2, 0, 0.1};
//+
Point(15) = {0.4, -0.2, 0, 0.1};
//+
Point(16) = {0.5, -0.3, 0, 0.1};
//+
Line(1) = {2, 3};
//+
Line(2) = {2, 5};
//+
Line(3) = {6, 9};
//+
Line(4) = {10, 1};
//+
Line(5) = {1, 4};
//+
Line(6) = {4, 16};
//+
Line(7) = {15, 13};
//+
Line(8) = {11, 3};
//+
Circle(9) = {5, 7, 6};
//+
Circle(10) = {9, 8, 10};
//+
Circle(11) = {16, 14, 15};
//+
Circle(12) = {13, 12, 11};
//+
Curve Loop(1) = {1, -8, -12, -7, -11, -6, -5, -4, -10, -3, -9, -2};
//+
Plane Surface(1) = {1};
//+
Physical Curve("cavern1", 11) = {9, 3, 10};
//+
Physical Curve("cavern2", 12) = {12, 7, 11};
//+
Physical Surface("domain", 1) = {1};
